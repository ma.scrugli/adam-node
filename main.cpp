//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* NOTE

	...

*/
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INCLUDE

#include "main.h"

#include <stdio.h>
#include <math.h>
#include <limits.h>

#include "cmsis_os.h"
#include "arm_math.h"
#include "arm_nnfunctions.h"

#include "stm32l4xx_hal_pwr.h"

//#include "datalog_application.h"		// REMOVE ME
//#include "TargetFeatures.h"			// REMOVE ME

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GENERAL DEFINE

	#define ENABLE_BLE
//	#define ENABLE_USB
//	#define ENABLE_LED


#define DATAQUEUE_SIZE  				((uint32_t) 50)

#define LED_ON 							BSP_LED_On(LED1)
#define LED_OFF 						BSP_LED_Off(LED1)
#define LED_TGG 						BSP_LED_Toggle(LED1)

#define timer_start()    				*((volatile uint32_t*)0xE0001000) = 0x40000001  						// Enable CYCCNT register
#define timer_stop()   					*((volatile uint32_t*)0xE0001000) = 0x40000000  						// Disable CYCCNT register
#define timer_get()   					*((volatile uint32_t*)0xE0001004)               						// Get value from CYCCNT register

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



typedef enum
{

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSORS

	 SENSOR_0 = 0						// ECG sensor
	 ,SENSOR_1
	 ,SENSOR_2
//	 ,NEW_SENSOR_N

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// VARIOUS

	,THREAD_BLE							// BLE								Task name = 	bleThread
	,THREAD_MASTER						// Master Thread					Task name = 	MasterThread
	,MESSAGE_MASTER						// Edit FIFO
	,MESSAGE_SENSOR						// ...
	,SEMAPHORE_BLE						// Timer semaphore
	,SEMAPHORE_MUTEX					// Semaphore mutex

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSOR_0

	,THREAD_GET_SENSOR_0				// Get ECG data						Task name = 	get_thread_0
	,THREAD_proc1_SENSOR_0				// ECG peak detection								proc1_thread_0
	,THREAD_proc2_SENSOR_0				// ECG data classification							proc2_thread_0
	,THREAD_THRSH_SENSOR_0				//													thrsh_thread_1
	,THREAD_SEND_SENSOR_0				// Send ECG data									send_thread_0

	,MESSAGE_proc1_SENSOR_0				// Peak FIFO
	,MESSAGE_proc2_SENSOR_0				// CNN FIFO
	,MESSAGE_THRSH_SENSOR_0				// Threshold FIFO
	,MESSAGE_SEND_SENSOR_0				// Send FIFO

	,SEMAPHORE_SENSOR_0					// Get data semaphore

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSOR_1

	,THREAD_GET_SENSOR_1				// Get temperature data				Task name = 	get_thread_1
//	,THREAD_proc1_SENSOR_1				// ...												proc1_thread_1
//	,THREAD_proc2_SENSOR_1				// ...												proc2_thread_1
	,THREAD_THRSH_SENSOR_1				// 													thrsh_thread_1
	,THREAD_SEND_SENSOR_1				// Send temperature data							send_thread_1

//	,MESSAGE_proc1_SENSOR_1				// ...
//	,MESSAGE_proc2_SENSOR_1				// ...
	,MESSAGE_THRSH_SENSOR_1				// Threshold FIFO
	,MESSAGE_SEND_SENSOR_1				// Send FIFO

	,SEMAPHORE_SENSOR_1					// Get data semaphore

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSOR_2

	,THREAD_GET_SENSOR_2				// Get pressure data				Task name = 	get_thread_2
//	,THREAD_proc1_SENSOR_2				// ...												proc1_thread_2
//	,THREAD_proc2_SENSOR_2				// ...												proc2_thread_2
	,THREAD_THRSH_SENSOR_2				// 													thrsh_thread_2
	,THREAD_SEND_SENSOR_2				// Send pressure data								send_thread_2

//	,MESSAGE_proc1_SENSOR_2				// ...
//	,MESSAGE_proc2_SENSOR_2				// ...
	,MESSAGE_THRSH_SENSOR_2				// Threshold FIFO
	,MESSAGE_SEND_SENSOR_2				// Send FIFO

	,SEMAPHORE_SENSOR_2					// Get data semaphore

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

} systemTypeDef;

typedef enum
{
	  TASK_SETUP = 0
	 ,SAMP_PERIOD_SETUP
	 ,SEND_PERIOD_SETUP
	 ,PERIOD_CHECK
} masterCommandTypeDef;

typedef enum
{
	 READ_LAST_VALUE = 0
	,READ_FROM_FIFO
} messageTypeTypeDef;

typedef enum
{
	 DO_NOT_SEND = 0
	,SEND
} thresholdTypeTypeDef;

typedef enum							// To be removed
{
	 ECG	 = 20						// ECG			   data type
	,DATA_1D = 7						// One-dimensional data type
	,DATA_3D = 10						// Tri-dimensional data type

} sensorDataTypeTypeDef;

typedef enum
{
	 INT16	 = 0
	,INT32
	,FLOAT

} DataTypeTypeDef;

typedef enum
{
	 RUN = 0
	,LPRUN
	,SLEEP_WFI
	,SLEEP_WFE
	,LPSLEEP_WFI
	,LPSLEEP_WFE
	,STOP0_WFI
	,STOP0_WFE
	,STOP1_WFI
	,STOP1_WFE
	,STOP2_WFI
	,STOP2_WFE
	,STANDBY
	,SHUTDOWN
} powerModeTypeTypeDef;

typedef struct
{
	int 				message_id;
	int 				sensor_id;

	uint32_t 			ms_counter;

	void 				*data;
	int					data_len;
} SensorsData;

struct sensorSetupStruct
{
	char 				name[20];
	char 				code[20];
	int				 	data_type;
	int					sensor_type;
	uint16_t 			charHandle;

	int 				en_threads[5];

	osThreadId 			get_threadId;
	osThreadId 			proc1_threadId;
	osThreadId 			proc2_threadId;
	osThreadId 			thrsh_threadId;
	osThreadId 			send_threadId;

	osMessageQId 		proc1_queueId;
	osMessageQId 		proc2_queueId;
	osMessageQId 		thrsh_queueId;
	osMessageQId 		send_queueId;

	osMessageQId 		out_get_queueId;
	osMessageQId 		out_proc1_queueId;
	osMessageQId 		out_proc2_queueId;
	osMessageQId 		out_thrsh_queueId;
	osMessageQId 		out_send_queueId;

	osSemaphoreId 		get_semId;

	osTimerId 			samp_timerId;
	int					samp_period_ms;
	void 				(*samp_timer_Callback) (void const *arg);

	osTimerId 			send_timerId;
	int					send_period_ms;
	void 				(*send_timer_Callback) (void const *arg);

	SensorsData 		*last_value;
	int					sample_packing;

	int			 		(*data_threshold) (SensorsData *data);

	void 				(*get_data) (SensorsData *data);
//	void 				(*init) (void);
//	void 				(*deInit) (void);
//	void 				(*enable) (void);
//	void 				(*disable) (void);
};



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MASTER

#define INIT_MASTER_TIMER_PERIOD			1000

static void 	master_thread(				void const *argument);

osThreadId									master_threadId;
osThreadDef(								THREAD_MASTER, master_thread, osPriorityAboveNormal, 0, configMINIMAL_STACK_SIZE*4);

osMessageQId 								master_queueId;
osMessageQDef(								MESSAGE_MASTER, DATAQUEUE_SIZE, int);

osPoolId 									thread_poolId;
osPoolDef(									thread_pool, DATAQUEUE_SIZE, int);

void 			master_timer_Callback(		void const *arg);
void 			master_timer_Start(			void);
void 			master_timer_Stop(			void);

osTimerId 									master_timerId;
osTimerDef(									master_timer, master_timer_Callback);

uint32_t									timer_period_master;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BLE

#define INIT_BLE_TIMER_PERIOD				500

static void 	ble_thread(					void const *argument);
static void 	InitBlueNRGStack(			void);

osThreadId									ble_threadId;
osThreadDef(								THREAD_BLE, ble_thread, osPriorityNormal, 	0, 	configMINIMAL_STACK_SIZE*4);

osSemaphoreId 								ble_semId;
osSemaphoreDef(								SEMAPHORE_BLE);

void 			ble_timer_Callback(			void const *arg);
void 			ble_timer_Start(			void);
void 			ble_timer_Stop(				void);

osTimerId 									ble_timerId;
osTimerDef(									ble_timer, ble_timer_Callback);

uint32_t									timer_period_ble;



// CUSTOM PARAMETERS & FUNCTIONS:



#include "sensor_service.h"
#include "bluenrg_utils.h"

static void 	GetBatteryInfoData(			void);

uint32_t 									StartTime;
uint32_t 									EndTime;
uint32_t									CycleTime;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VARIOUS



#define NUMBER_OF_SENSORS					3
#define INIT_TASK_STATE						0b000000


void 			init_all_task(				void);
void 			*dataMalloc(				int type, int size);
void 			dataCopy(					int type, int *cnt, SensorsData *src, SensorsData *dst);

void 			Error_Handler(				void);

static void 	gp_read_from_sensor(		int sensorId);
static void 	gp_data_threshold(			int sensorId);
static void 	gp_send_to_gateway(			int sensorId);

//extern void 	vPortSetupTimerInterrupt( 	void);



osPoolId 									message_poolId;
osPoolDef(									MESSAGE_SENSOR, 		DATAQUEUE_SIZE, 	SensorsData);

osSemaphoreId 								mutex_semId;
osSemaphoreDef(								SEMAPHORE_MUTEX);

uint32_t									soc;
int32_t										current= 0;
uint32_t									voltage;

uint32_t 									timer_0;
uint32_t 									timer_1;
uint32_t 									timer_2;
uint32_t 									timer_3;
uint32_t 									timer_4;
uint32_t 									timer_5;
uint32_t 									timer_6;
uint32_t 									timer_7;
uint32_t 									timer_8;
uint32_t 									timer_9;



extern uint32_t 							ulTimerCountsForOneTick;
extern uint8_t 								set_connectable;
extern int 									connected;
static volatile uint32_t 					HCI_ProcessEvent = 		0;

uint32_t									taskState = 			INIT_TASK_STATE;

uint8_t 									BufferToWrite[256];
int32_t 									BytesToWrite;
uint8_t 									bdaddr[6];
uint32_t 									ConnectionBleStatus = 	0;
uint32_t 									exec;
USBD_HandleTypeDef 							USBD_Device;

uint32_t									samp_period_ms_index;
uint32_t									samp_period_ms_value;

uint32_t									send_period_ms_index;
uint32_t									send_period_ms_value;

struct sensorSetupStruct 					sensorSetup[NUMBER_OF_SENSORS];



// CUSTOM PARAMETERS & FUNCTIONS:



void			powerControl(				int mode, int frequency);

static void 	GetBatteryInfoData(			void);
void *STC3115_handle = 						NULL;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_0

	static void 	get_thread_0(				void const *argument);
	static void 	proc1_thread_0(				void const *argument);
//	static void 	proc2_thread_0(				void const *argument);
	static void 	thrsh_thread_0(				void const *argument);
	static void 	send_thread_0(				void const *argument);

	osMessageQDef(								MESSAGE_proc1_SENSOR_0, DATAQUEUE_SIZE, 	int);
//	osMessageQDef(								MESSAGE_proc2_SENSOR_0, DATAQUEUE_SIZE, 	int);
	osMessageQDef(								MESSAGE_THRSH_SENSOR_0, DATAQUEUE_SIZE, 	int);
	osMessageQDef(								MESSAGE_SEND_SENSOR_0, 	DATAQUEUE_SIZE, 	int);

	osSemaphoreDef(								SEMAPHORE_SENSOR_0);

	osThreadDef(								THREAD_GET_SENSOR_0,	get_thread_0,		osPriorityAboveNormal,	0,	configMINIMAL_STACK_SIZE*2);
	osThreadDef(								THREAD_proc1_SENSOR_0,	proc1_thread_0,		osPriorityAboveNormal,	0,	configMINIMAL_STACK_SIZE*2);
//	osThreadDef(								THREAD_proc2_SENSOR_0,	proc2_thread_0,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*55);
	osThreadDef(								THREAD_THRSH_SENSOR_0,	thrsh_thread_0,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*2);
	osThreadDef(								THREAD_SEND_SENSOR_0,	send_thread_0,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*4);

	void 			samp_timer_0_Callback(		void const *arg);
	void 			send_timer_0_Callback(		void const *arg);



// CUSTOM PARAMETERS & FUNCTIONS:



	#include "ecg_data.h"
	#include "ds_cnn.h"

	void 			ecg_read_from_sensor(		SensorsData *data);
	int			 	ecg_data_threshold(			SensorsData *data);

	static void 	ecg_peak_detector(			int sensorId);
	static void 	ecg_cnn_classifier(			int sensorId);

//	static void 	enable_1(					void);
//	static void 	disable_1(					void);
//	static void 	setOdr_1(					void);

	#define ECG_SAMP_PERIOD_VALUE			3
	#define HB_SEND_PERIOD_VALUE				1000



	#define TEST_IN_DIM							300
	#define TEST_IF								1
	#define TEST_OF								18
	#define TEST_K_DIM							7
	#define TEST_STRIDE							1
	#define TEST_PADDING						0
	#define TEST_DENSE							100

	#define TEST_BIAS_LSHIFT					2
	#define TEST_OUT_RSHIFT						6

//	#define TEST_BIT_DEPTH_8
//	#define TEST_BIT_DEPTH_16

#ifdef TEST_BIT_DEPTH_8
	const q7_t test_final_fc_wt[(TEST_IN_DIM/4)*TEST_OF*TEST_DENSE] = {-1};
#endif

#ifdef TEST_BIT_DEPTH_16
	const q15_t test_final_fc_wt_large[(TEST_IN_DIM/4)*TEST_OF*TEST_DENSE] = {-1};
#endif



	int16_t 									ecg_data[ECG_SAMPLE] = 	ECG_DATA;
	int											ecg_delta = 			ECG_DELTA;
	int											ecg_debounce = 			ECG_RATE/6;
	int											ecg_rate = 				60000/ECG_SAMP_PERIOD_VALUE;
	int32_t										ecg_count = 			0;
	int32_t										ecg_prev_count = 		0;
	int32_t										ecg_count_peak = 		-ecg_debounce;
	int16_t										ecg_prev_data = 		ecg_data[0];
	int16_t										ecg_heartbeat = 		0;
	int											ecg_state = 			0;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_1

	static void 	get_thread_1(				void const *argument);
//	static void 	proc1_thread_1(				void const *argument);
//	static void 	proc2_thread_1(				void const *argument);
	static void 	thrsh_thread_1(				void const *argument);
	static void 	send_thread_1(				void const *argument);

//	osMessageQDef(								MESSAGE_proc1_SENSOR_1, DATAQUEUE_SIZE, 	int);
//	osMessageQDef(								MESSAGE_proc2_SENSOR_1, DATAQUEUE_SIZE, 	int);
	osMessageQDef(								MESSAGE_THRSH_SENSOR_1, DATAQUEUE_SIZE, 	int);
	osMessageQDef(								MESSAGE_SEND_SENSOR_1, 	DATAQUEUE_SIZE, 	int);

	osSemaphoreDef(								SEMAPHORE_SENSOR_1);

	osThreadDef(								THREAD_GET_SENSOR_1,	get_thread_1,		osPriorityAboveNormal,	0,	configMINIMAL_STACK_SIZE*2);
//	osThreadDef(								THREAD_proc1_SENSOR_1,	proc1_thread_1,		osPriorityAboveNormal,	0,	configMINIMAL_STACK_SIZE*2);
//	osThreadDef(								THREAD_proc2_SENSOR_1,	proc2_thread_1,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*55);
	osThreadDef(								THREAD_THRSH_SENSOR_1,	thrsh_thread_1,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*2);
	osThreadDef(								THREAD_SEND_SENSOR_1,	send_thread_1,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*4);

	void 			samp_timer_1_Callback(		void const *arg);
	void 			send_timer_1_Callback(		void const *arg);



// CUSTOM PARAMETERS & FUNCTIONS:



	void 			temp_read_from_sensor(		SensorsData *data);
	int 		 	temp_data_threshold(		SensorsData *data);

//	static void 	enable_1(					void);
//	static void 	disable_1(					void);
//	static void 	setOdr_1(					void);



	#define TEMP_SAMP_PERIOD_VALUE				1000
	#define TEMP_SEND_PERIOD_VALUE				1000

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_2

	static void 	get_thread_2(				void const *argument);
//	static void 	proc1_thread_2(				void const *argument);
//	static void 	proc2_thread_2(				void const *argument);
	static void 	thrsh_thread_2(				void const *argument);
	static void 	send_thread_2(				void const *argument);

//	osMessageQDef(								MESSAGE_proc1_SENSOR_2, DATAQUEUE_SIZE, 	int);
//	osMessageQDef(								MESSAGE_proc2_SENSOR_2, DATAQUEUE_SIZE, 	int);
	osMessageQDef(								MESSAGE_THRSH_SENSOR_2, DATAQUEUE_SIZE, 	int);
	osMessageQDef(								MESSAGE_SEND_SENSOR_2, 	DATAQUEUE_SIZE, 	int);

	osSemaphoreDef(								SEMAPHORE_SENSOR_2);

	osThreadDef(								THREAD_GET_SENSOR_2,	get_thread_2,		osPriorityAboveNormal,	0,	configMINIMAL_STACK_SIZE*2);
//	osThreadDef(								THREAD_proc1_SENSOR_2,	proc1_thread_2,		osPriorityAboveNormal,	0,	configMINIMAL_STACK_SIZE*2);
//	osThreadDef(								THREAD_proc2_SENSOR_2,	proc2_thread_2,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*55);
	osThreadDef(								THREAD_THRSH_SENSOR_2,	thrsh_thread_2,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*2);
	osThreadDef(								THREAD_SEND_SENSOR_2,	send_thread_2,		osPriorityNormal,		0,	configMINIMAL_STACK_SIZE*4);

	void 			samp_timer_2_Callback(		void const *arg);
	void 			send_timer_2_Callback(		void const *arg);



// CUSTOM PARAMETERS & FUNCTIONS:



	void 			pres_read_from_sensor(		SensorsData *data);
	int 			pres_data_threshold(		SensorsData *data);

//	static void 	enable_2(					void);
//	static void 	disable_2(					void);
//	static void 	setOdr_2(					void);



	#define PRES_SAMP_PERIOD_VALUE				1000
	#define PRES_SEND_PERIOD_VALUE				1000

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////






int main(void)
{

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MAIN

	HAL_Init();

	SystemClock_Config();

    BSP_LED_Init(LED1);

	HAL_PWREx_EnableVddUSB();													// Enable USB power on Pwrctrl CR2 register

	USBD_Init(								&USBD_Device, &VCP_Desc, 0);		// Init Device Library
	USBD_RegisterClass(						&USBD_Device, USBD_CDC_CLASS);		// Add Supported Class
	USBD_CDC_RegisterInterface(				&USBD_Device, &USBD_CDC_fops);		// Add Interface Callbacks for AUDIO and CDC Class
	USBD_Start(								&USBD_Device);						// Start Device Process

 	InitTargetPlatform(TARGET_SENSORTILE);

	InitBlueNRGStack();

	Add_ConfigW2ST_Service();
	Add_HWServW2ST_Service();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MASTER

	master_threadId = 					osThreadCreate(		osThread(	THREAD_MASTER), 		NULL);
	master_queueId = 					osMessageCreate(	osMessageQ(	MESSAGE_MASTER), 		NULL);

	timer_period_master =				INIT_MASTER_TIMER_PERIOD;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// BLE

	ble_threadId = 						osThreadCreate(		osThread(	THREAD_BLE), 		NULL);
	ble_semId = 						osSemaphoreCreate(	osSemaphore(SEMAPHORE_BLE), 	1);

	timer_period_ble =					INIT_BLE_TIMER_PERIOD;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ALL SENSORS

	thread_poolId = 					osPoolCreate(		osPool(		thread_pool));
	message_poolId = 					osPoolCreate(		osPool(		MESSAGE_SENSOR));

	mutex_semId = 						osSemaphoreCreate(	osSemaphore(SEMAPHORE_MUTEX), 		1);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSOR_0

	sprintf(sensorSetup[SENSOR_0].name,			"ECG sensor");
	sprintf(sensorSetup[SENSOR_0].code,			"...");
	sensorSetup[SENSOR_0].data_type =			INT16;
	sensorSetup[SENSOR_0].sensor_type =			ECG;


	sensorSetup[SENSOR_0].en_threads[0] =		1;
	sensorSetup[SENSOR_0].en_threads[1] =		1;
	sensorSetup[SENSOR_0].en_threads[2] =		0;
	sensorSetup[SENSOR_0].en_threads[3] =		1;
	sensorSetup[SENSOR_0].en_threads[4] =		1;

	sensorSetup[SENSOR_0].get_threadId =		osThreadCreate(		osThread(	THREAD_GET_SENSOR_0		), 	NULL);
	sensorSetup[SENSOR_0].proc1_threadId =		osThreadCreate(		osThread(	THREAD_proc1_SENSOR_0	), 	NULL);
//	sensorSetup[SENSOR_0].proc2_threadId =		osThreadCreate(		osThread(	THREAD_proc2_SENSOR_0	), 	NULL);
	sensorSetup[SENSOR_0].thrsh_threadId =		osThreadCreate(		osThread(	THREAD_THRSH_SENSOR_0	), 	NULL);
	sensorSetup[SENSOR_0].send_threadId =		osThreadCreate(		osThread(	THREAD_SEND_SENSOR_0	), 	NULL);

	sensorSetup[SENSOR_0].proc1_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_proc1_SENSOR_0	), NULL);
//	sensorSetup[SENSOR_0].proc2_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_proc2_SENSOR_0	), NULL);
	sensorSetup[SENSOR_0].thrsh_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_THRSH_SENSOR_0	), NULL);
	sensorSetup[SENSOR_0].send_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_SEND_SENSOR_0	), NULL);

	sensorSetup[SENSOR_0].get_semId = 			osSemaphoreCreate(	osSemaphore(SEMAPHORE_SENSOR_0), 	1);

	sensorSetup[SENSOR_0].samp_timer_Callback = samp_timer_0_Callback;
	osTimerDef( 								TIMER_SAMP_SENSOR_0, sensorSetup[SENSOR_0].samp_timer_Callback);
	sensorSetup[SENSOR_0].samp_timerId = 		osTimerCreate(osTimer(TIMER_SAMP_SENSOR_0), osTimerPeriodic, &exec);

	sensorSetup[SENSOR_0].send_timer_Callback = send_timer_0_Callback;
	osTimerDef( 								TIMER_SEND_SENSOR_0, sensorSetup[SENSOR_0].send_timer_Callback);
	sensorSetup[SENSOR_0].send_timerId = 		osTimerCreate(osTimer(TIMER_SEND_SENSOR_0), osTimerPeriodic, &exec);

	sensorSetup[SENSOR_0].last_value = 			(SensorsData *) pvPortMalloc(sizeof(SensorsData) * 1);
	sensorSetup[SENSOR_0].sample_packing = 		1;
	sensorSetup[SENSOR_0].last_value->data = 	dataMalloc(sensorSetup[SENSOR_0].data_type, sensorSetup[SENSOR_0].sample_packing);

	sensorSetup[SENSOR_0].charHandle = 			addSensorCharacteristc(SENSOR_0, sensorSetup[SENSOR_0].sensor_type);

	sensorSetup[SENSOR_0].data_threshold = 		ecg_data_threshold;

	sensorSetup[SENSOR_0].get_data = 			ecg_read_from_sensor;
//	sensorSetup[SENSOR_0].enable = 				enable_0;
//	sensorSetup[SENSOR_0].disable = 			disable_0;

	sensorSetup[SENSOR_0].samp_period_ms =		ECG_SAMP_PERIOD_VALUE;
	sensorSetup[SENSOR_0].send_period_ms =		HB_SEND_PERIOD_VALUE;


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSOR_1

	sprintf(sensorSetup[SENSOR_1].name,			"Temperature sensor");
	sprintf(sensorSetup[SENSOR_1].code,			"...");
	sensorSetup[SENSOR_1].data_type =			FLOAT;
	sensorSetup[SENSOR_1].sensor_type =			DATA_1D;

	sensorSetup[SENSOR_1].en_threads[0] =		1;
	sensorSetup[SENSOR_1].en_threads[1] =		0;
	sensorSetup[SENSOR_1].en_threads[2] =		0;
	sensorSetup[SENSOR_1].en_threads[3] =		1;
	sensorSetup[SENSOR_1].en_threads[4] =		1;

	sensorSetup[SENSOR_1].get_threadId =		osThreadCreate(		osThread(	THREAD_GET_SENSOR_1		), 	NULL);
//	sensorSetup[SENSOR_1].proc1_threadId =		osThreadCreate(		osThread(	THREAD_proc1_SENSOR_1	), 	NULL);
//	sensorSetup[SENSOR_1].proc2_threadId =		osThreadCreate(		osThread(	THREAD_proc2_SENSOR_1	), 	NULL);
	sensorSetup[SENSOR_1].thrsh_threadId =		osThreadCreate(		osThread(	THREAD_THRSH_SENSOR_1	), 	NULL);
	sensorSetup[SENSOR_1].send_threadId =		osThreadCreate(		osThread(	THREAD_SEND_SENSOR_1	), 	NULL);

//	sensorSetup[SENSOR_1].proc1_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_proc1_SENSOR_1	), NULL);
//	sensorSetup[SENSOR_1].proc2_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_proc2_SENSOR_1	), NULL);
	sensorSetup[SENSOR_1].thrsh_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_THRSH_SENSOR_1	), NULL);
	sensorSetup[SENSOR_1].send_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_SEND_SENSOR_1	), NULL);

	sensorSetup[SENSOR_1].get_semId = 			osSemaphoreCreate(	osSemaphore(SEMAPHORE_SENSOR_1), 	1);

	sensorSetup[SENSOR_1].samp_timer_Callback = samp_timer_1_Callback;
	osTimerDef( 								TIMER_SAMP_SENSOR_1, sensorSetup[SENSOR_1].samp_timer_Callback);
	sensorSetup[SENSOR_1].samp_timerId = 		osTimerCreate(osTimer(TIMER_SAMP_SENSOR_1), osTimerPeriodic, &exec);

	sensorSetup[SENSOR_1].send_timer_Callback = send_timer_1_Callback;
	osTimerDef( 								TIMER_SEND_SENSOR_1, sensorSetup[SENSOR_1].send_timer_Callback);
	sensorSetup[SENSOR_1].send_timerId = 		osTimerCreate(osTimer(TIMER_SEND_SENSOR_1), osTimerPeriodic, &exec);

	sensorSetup[SENSOR_1].last_value = 			(SensorsData *) pvPortMalloc(sizeof(SensorsData) * 1);
	sensorSetup[SENSOR_1].sample_packing = 		1;
	sensorSetup[SENSOR_1].last_value->data = 	dataMalloc(sensorSetup[SENSOR_1].data_type, sensorSetup[SENSOR_1].sample_packing);

	sensorSetup[SENSOR_1].charHandle = 			addSensorCharacteristc(SENSOR_1, sensorSetup[SENSOR_1].sensor_type);

	sensorSetup[SENSOR_1].data_threshold = 		temp_data_threshold;

	sensorSetup[SENSOR_1].get_data = 			temp_read_from_sensor;
//	sensorSetup[SENSOR_1].enable = 				enable_1;
//	sensorSetup[SENSOR_1].disable = 			disable_1;

	sensorSetup[SENSOR_1].samp_period_ms =		TEMP_SAMP_PERIOD_VALUE;
	sensorSetup[SENSOR_1].send_period_ms =		TEMP_SEND_PERIOD_VALUE;


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// SENSOR_2

	sprintf(sensorSetup[SENSOR_2].name,			"Pressure sensor");
	sprintf(sensorSetup[SENSOR_2].code,			"...");
	sensorSetup[SENSOR_2].data_type =			FLOAT;
	sensorSetup[SENSOR_2].sensor_type =			DATA_1D;

	sensorSetup[SENSOR_2].en_threads[0] =		1;
	sensorSetup[SENSOR_2].en_threads[1] =		0;
	sensorSetup[SENSOR_2].en_threads[2] =		0;
	sensorSetup[SENSOR_2].en_threads[3] =		1;
	sensorSetup[SENSOR_2].en_threads[4] =		1;

	sensorSetup[SENSOR_2].get_threadId =		osThreadCreate(		osThread(	THREAD_GET_SENSOR_2		), 	NULL);
//	sensorSetup[SENSOR_2].proc1_threadId =		osThreadCreate(		osThread(	THREAD_proc1_SENSOR_2	), 	NULL);
//	sensorSetup[SENSOR_2].proc2_threadId =		osThreadCreate(		osThread(	THREAD_proc2_SENSOR_2	), 	NULL);
	sensorSetup[SENSOR_2].thrsh_threadId =		osThreadCreate(		osThread(	THREAD_THRSH_SENSOR_2	), 	NULL);
	sensorSetup[SENSOR_2].send_threadId =		osThreadCreate(		osThread(	THREAD_SEND_SENSOR_2	), 	NULL);

//	sensorSetup[SENSOR_2].proc1_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_proc1_SENSOR_2	), NULL);
//	sensorSetup[SENSOR_2].proc2_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_proc2_SENSOR_2	), NULL);
	sensorSetup[SENSOR_2].thrsh_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_THRSH_SENSOR_2	), NULL);
	sensorSetup[SENSOR_2].send_queueId = 		osMessageCreate(	osMessageQ(	MESSAGE_SEND_SENSOR_2	), NULL);

	sensorSetup[SENSOR_2].get_semId = 			osSemaphoreCreate(	osSemaphore(SEMAPHORE_SENSOR_2), 	1);

	sensorSetup[SENSOR_2].samp_timer_Callback = samp_timer_2_Callback;
	osTimerDef( 								TIMER_SAMP_SENSOR_2, sensorSetup[SENSOR_2].samp_timer_Callback);
	sensorSetup[SENSOR_2].samp_timerId = 		osTimerCreate(osTimer(TIMER_SAMP_SENSOR_2), osTimerPeriodic, &exec);

	sensorSetup[SENSOR_2].send_timer_Callback = send_timer_2_Callback;
	osTimerDef( 								TIMER_SEND_SENSOR_2, sensorSetup[SENSOR_2].send_timer_Callback);
	sensorSetup[SENSOR_2].send_timerId = 		osTimerCreate(osTimer(TIMER_SEND_SENSOR_2), osTimerPeriodic, &exec);

	sensorSetup[SENSOR_2].last_value = 			(SensorsData *) pvPortMalloc(sizeof(SensorsData) * 1);
	sensorSetup[SENSOR_2].sample_packing = 		1;
	sensorSetup[SENSOR_2].last_value->data = 	dataMalloc(sensorSetup[SENSOR_2].data_type, sensorSetup[SENSOR_2].sample_packing);

	sensorSetup[SENSOR_2].charHandle = 			addSensorCharacteristc(SENSOR_2, sensorSetup[SENSOR_2].sensor_type);

	sensorSetup[SENSOR_2].data_threshold = 		pres_data_threshold;

	sensorSetup[SENSOR_2].get_data = 			pres_read_from_sensor;
//	sensorSetup[SENSOR_2].enable = 				enable_2;
//	sensorSetup[SENSOR_2].disable = 			disable_2;

	sensorSetup[SENSOR_2].samp_period_ms =		TEMP_SAMP_PERIOD_VALUE;
	sensorSetup[SENSOR_2].send_period_ms =		TEMP_SEND_PERIOD_VALUE;


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////






	osKernelStart();					// Start scheduler

	for (;;);							// We should never get here as control is now taken by the scheduler
}

static void master_thread(void const *argument)
{
	(void) argument;

	Sensor_IO_SPI_CS_Init_All();		// Configure and disable all the Chip Select pins

	for (int i = 0; i < NUMBER_OF_SENSORS; i++)
	{
		if(sensorSetup[i].en_threads[0]) osThreadSuspend(sensorSetup[i].get_threadId);
		if(sensorSetup[i].en_threads[1]) osThreadSuspend(sensorSetup[i].proc1_threadId);
		if(sensorSetup[i].en_threads[2]) osThreadSuspend(sensorSetup[i].proc2_threadId);
		if(sensorSetup[i].en_threads[3]) osThreadSuspend(sensorSetup[i].thrsh_threadId);
		if(sensorSetup[i].en_threads[4]) osThreadSuspend(sensorSetup[i].send_threadId);
	}

	osEvent evt;
	int *message;

	init_all_task();

	master_timer_Start();
	ble_timer_Start();



	for (;;)
	{
		evt = osMessageGet(master_queueId, osWaitForever);

		if (evt.status == osEventMessage)
		{
			message = (int *) evt.value.p;

			switch (*message)
			{
				case TASK_SETUP:

					for (int i = 0; i < NUMBER_OF_SENSORS; i++)
					{
						switch ((taskState & (3 << (2*i))) >> (2*i))
						{
							case (0):
								osTimerStop( sensorSetup[i].samp_timerId);
								osTimerStop( sensorSetup[i].send_timerId);

								if(sensorSetup[i].en_threads[0]) osThreadSuspend(sensorSetup[i].get_threadId);
								if(sensorSetup[i].en_threads[1]) osThreadSuspend(sensorSetup[i].proc1_threadId);
								if(sensorSetup[i].en_threads[2]) osThreadSuspend(sensorSetup[i].proc2_threadId);
								if(sensorSetup[i].en_threads[3]) osThreadSuspend(sensorSetup[i].thrsh_threadId);
								if(sensorSetup[i].en_threads[4]) osThreadSuspend(sensorSetup[i].send_threadId);
							break;

							case (1):
								osTimerStart(sensorSetup[i].samp_timerId, sensorSetup[i].samp_period_ms);
								osTimerStop( sensorSetup[i].send_timerId);

								sensorSetup[i].out_get_queueId = 	sensorSetup[i].thrsh_queueId;
								sensorSetup[i].out_thrsh_queueId = 	sensorSetup[i].send_queueId;

								if (i != SENSOR_0) sensorSetup[i].sample_packing = 1;
								if (i == SENSOR_0) sensorSetup[i].sample_packing = 8;

								if(sensorSetup[i].en_threads[0]) osThreadResume(sensorSetup[i].get_threadId);
								if(sensorSetup[i].en_threads[1]) osThreadSuspend(sensorSetup[i].proc1_threadId);
								if(sensorSetup[i].en_threads[2]) osThreadSuspend(sensorSetup[i].proc2_threadId);
								if(sensorSetup[i].en_threads[3]) osThreadResume(sensorSetup[i].thrsh_threadId);
								if(sensorSetup[i].en_threads[4]) osThreadResume(sensorSetup[i].send_threadId);
							break;

							case (2):
								osTimerStart(sensorSetup[i].samp_timerId, sensorSetup[i].samp_period_ms);
								if (i == SENSOR_0) osTimerStart(sensorSetup[i].send_timerId, sensorSetup[i].send_period_ms);

								sensorSetup[i].out_get_queueId = 	sensorSetup[i].proc1_queueId;
								sensorSetup[i].out_proc1_queueId = 	sensorSetup[i].thrsh_queueId;
								sensorSetup[i].out_thrsh_queueId = 	sensorSetup[i].send_queueId;

								sensorSetup[i].sample_packing = 1;

								if(sensorSetup[i].en_threads[0]) osThreadResume(sensorSetup[i].get_threadId);
								if(sensorSetup[i].en_threads[1]) osThreadResume(sensorSetup[i].proc1_threadId);
								if(sensorSetup[i].en_threads[2]) osThreadSuspend(sensorSetup[i].proc2_threadId);
								if(sensorSetup[i].en_threads[3]) osThreadResume(sensorSetup[i].thrsh_threadId);
								if(sensorSetup[i].en_threads[4]) osThreadResume(sensorSetup[i].send_threadId);

							break;

							case (3):
								osTimerStart(sensorSetup[i].samp_timerId, sensorSetup[i].samp_period_ms);
								osTimerStop( sensorSetup[i].send_timerId);

								sensorSetup[i].out_get_queueId = 	sensorSetup[i].proc1_queueId;
								sensorSetup[i].out_proc1_queueId = 	sensorSetup[i].proc2_queueId;
								sensorSetup[i].out_proc2_queueId = 	sensorSetup[i].thrsh_queueId;
								sensorSetup[i].out_thrsh_queueId = 	sensorSetup[i].send_queueId;

								if(sensorSetup[i].en_threads[0]) osThreadResume(sensorSetup[i].get_threadId);
								if(sensorSetup[i].en_threads[1]) osThreadResume(sensorSetup[i].proc1_threadId);
								if(sensorSetup[i].en_threads[2]) osThreadResume(sensorSetup[i].proc2_threadId);
								if(sensorSetup[i].en_threads[3]) osThreadResume(sensorSetup[i].thrsh_threadId);
								if(sensorSetup[i].en_threads[4]) osThreadResume(sensorSetup[i].send_threadId);
							break;

							default:
								osTimerStop( sensorSetup[i].samp_timerId);
								osTimerStop( sensorSetup[i].send_timerId);

								if(sensorSetup[i].en_threads[0]) osThreadSuspend(sensorSetup[i].get_threadId);
								if(sensorSetup[i].en_threads[1]) osThreadSuspend(sensorSetup[i].proc1_threadId);
								if(sensorSetup[i].en_threads[2]) osThreadSuspend(sensorSetup[i].proc2_threadId);
								if(sensorSetup[i].en_threads[3]) osThreadSuspend(sensorSetup[i].thrsh_threadId);
								if(sensorSetup[i].en_threads[4]) osThreadSuspend(sensorSetup[i].send_threadId);
							break;
						}
					}

					break;

				case SAMP_PERIOD_SETUP:

					sensorSetup[samp_period_ms_index].samp_period_ms = samp_period_ms_value;
					init_all_task();

					break;

				case SEND_PERIOD_SETUP:

					sensorSetup[send_period_ms_index].send_period_ms = send_period_ms_value;
					init_all_task();

					break;

				case PERIOD_CHECK:

					if(0) GetBatteryInfoData();

					break;

				default:

					// ...

					break;

			}

			osPoolFree(thread_poolId, message);
		}
	}
}

static void ble_thread(	void const *argument)
{

	uint32_t StartTime;

	StartTime = osKernelSysTick();

	for (;;)
	{
		osSemaphoreWait(ble_semId, osWaitForever);

		if (!connected)
		{
			if (!TargetBoardFeatures.LedStatus)
			{
				if (osKernelSysTick()-StartTime > (float)1000)
				{
					StartTime = osKernelSysTick();
				}
			}
			else
			{
				if (osKernelSysTick()-StartTime > (float)100)
				{
					StartTime = osKernelSysTick();
				}
			}
		}

		if (HCI_ProcessEvent)							// Handle BLE event
		{
			HCI_ProcessEvent=0;
			HCI_Process();
		}

		if (set_connectable)							// Update the BLE advertise data and make the Board connectable
		{
			setConnectable();
			set_connectable = FALSE;
		}

//	    __WFI();
	}
}









//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_0

	static void get_thread_0(	void const *argument) {	gp_read_from_sensor(	SENSOR_0 ); }
	static void proc1_thread_0(	void const *argument) { ecg_peak_detector(		SENSOR_0 ); }
//	static void proc2_thread_0(	void const *argument) { ecg_cnn_classifier(		SENSOR_0 ); }
	static void thrsh_thread_0(	void const *argument) { gp_data_threshold(		SENSOR_0 ); }
	static void send_thread_0(	void const *argument) { gp_send_to_gateway(		SENSOR_0 ); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_1

	static void get_thread_1(	void const *argument) {	gp_read_from_sensor(	SENSOR_1 ); }
//	static void proc1_thread_1(	void const *argument) { _proc0(					SENSOR_1 ); }
//	static void proc2_thread_1(	void const *argument) { _proc1(					SENSOR_1 ); }
	static void thrsh_thread_1(	void const *argument) { gp_data_threshold(		SENSOR_1 ); }
	static void send_thread_1(	void const *argument) { gp_send_to_gateway(		SENSOR_1 ); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_2

	static void get_thread_2(	void const *argument) {	gp_read_from_sensor(	SENSOR_2 ); }
//	static void proc1_thread_2(	void const *argument) { _proc0(					SENSOR_2 ); }
//	static void proc2_thread_2(	void const *argument) { _proc1(					SENSOR_2 ); }
	static void thrsh_thread_2(	void const *argument) { gp_data_threshold(		SENSOR_2 ); }
	static void send_thread_2(	void const *argument) { gp_send_to_gateway(		SENSOR_2 ); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////









//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GENERAL PURPOSE FUNCTIONS

static void gp_read_from_sensor(int sensorId)
{
	SensorsData *mptr;

	for (;;)
	{
		osSemaphoreWait(sensorSetup[sensorId].get_semId, osWaitForever);

		mptr = (SensorsData *) pvPortMalloc(sizeof(SensorsData) * 1);
		mptr->data = dataMalloc(sensorSetup[sensorId].data_type, mptr->data_len);

		if (mptr != NULL)
		{
			sensorSetup[sensorId].get_data(mptr);

			mptr->ms_counter = osKernelSysTick();
			mptr->message_id = READ_FROM_FIFO;
//			memcpy(sensorSetup[sensorId].last_value, mptr, sizeof(SensorsData));
//			... (data copy)
		}
		if (osMessagePut(sensorSetup[sensorId].out_get_queueId, (uint32_t) mptr, osWaitForever) != osOK) Error_Handler();
	}
}

static void gp_data_threshold(int sensorId)
{
	osEvent evt;
	SensorsData *mptr;
	SensorsData *rptr;

	int sendCnt = 0;

	for (;;)
	{
		evt = 	osMessageGet(sensorSetup[sensorId].thrsh_queueId, osWaitForever);

		if (evt.status == osEventMessage)
		{

			rptr = (SensorsData *) evt.value.p;

			if(!sendCnt)
			{
				mptr = 					(SensorsData *) pvPortMalloc(sizeof(SensorsData) * 1);
				mptr->data_len = 		sensorSetup[sensorId].sample_packing;
				sendCnt = 				mptr->data_len;
				mptr->data = 			dataMalloc(sensorSetup[sensorId].data_type, mptr->data_len);
			}

			dataCopy(sensorSetup[sensorId].data_type, &sendCnt, rptr, mptr);

			if(!sendCnt)
			{
				if(sensorSetup[sensorId].data_threshold(mptr) == SEND)
				{
					mptr->ms_counter = rptr->ms_counter;
					if (osMessagePut(sensorSetup[sensorId].out_thrsh_queueId, (uint32_t) mptr, osWaitForever) != osOK) Error_Handler();
				}
			}

		}

		if(rptr->message_id == READ_FROM_FIFO)
		{
			vPortFree(rptr->data);
			vPortFree(rptr);
		}

	}
}

static void gp_send_to_gateway(int sensorId)
{
	osEvent evt;
	SensorsData *rptr;

	for (;;)
	{
		evt = 	osMessageGet(sensorSetup[sensorId].send_queueId, osWaitForever);

		if (evt.status == osEventMessage)
		{
			rptr = (SensorsData *) evt.value.p;

			charUpdate(
						sensorSetup[sensorId].charHandle,
						sensorSetup[sensorId].data_type,
						sensorSetup[sensorId].sensor_type,
						rptr->ms_counter,
						rptr->data,
						rptr->data_len
					  );

		}

		vPortFree(rptr->data);
		vPortFree(rptr);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////









//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_0: FUNCTIONS & TIMER

void ecg_read_from_sensor(SensorsData *packet)
{
	packet->data_len = 				1;
	*((int16_t*) packet->data) = 	ecg_data[ecg_count % ECG_SAMPLE];
	ecg_prev_count = 				ecg_count++;
}

static void ecg_peak_detector(int sensorId)
{
	osEvent evt;
	SensorsData *rptr;

	for (;;)
	{
		evt = osMessageGet(sensorSetup[sensorId].proc1_queueId, osWaitForever);

		if (evt.status == osEventMessage)
		{
			rptr = (SensorsData *) evt.value.p;

			switch (ecg_state & 0b11)
			{
				case (0):
					if(((*((int16_t*) rptr->data) - ecg_prev_data) > ecg_delta) && ((ecg_prev_count - ecg_count_peak) > ecg_debounce)) ecg_state++;
				break;

				case (1):
//					LED_ON;
					if(*((int16_t*) rptr->data) < ecg_prev_data)
					{
						ecg_heartbeat = ecg_rate/((ecg_prev_count - 1) - ecg_count_peak);
						ecg_count_peak = ecg_prev_count - 1;
						ecg_state++;
					}
				break;

				case (2):
					*((int16_t*) sensorSetup[sensorId].last_value->data) = 	ecg_heartbeat;
					sensorSetup[sensorId].last_value->ms_counter = 			osKernelSysTick();
					sensorSetup[sensorId].last_value->message_id = 			READ_LAST_VALUE;

					ecg_state++;
				break;

				case (3):
					if((*((int16_t*) rptr->data) - ecg_prev_data) > 0)
					{
						ecg_state++;
//						LED_OFF;
					}
				break;

				default:
					while(1);
				break;
			}

			ecg_prev_data = *((int16_t*) rptr->data);


			vPortFree(rptr->data);
			vPortFree(rptr);
		}
	}
}

static void ecg_cnn_classifier(int sensorId)
{
	osEvent evt;
	SensorsData *rptr;

	#ifdef TEST_BIT_DEPTH_8
		q7_t byte_array[TEST_IN_DIM*TEST_OF] = {-1};

		const q7_t test_wt[TEST_OF*TEST_IF*TEST_OF*TEST_K_DIM] = {-1};
		const q7_t test_bias[TEST_OF*TEST_IF*TEST_OF] = {-1};
		const q7_t test_final_fc_bias[TEST_DENSE] = {-1};

		q7_t* test_buffer;
		q7_t* test_col_buffer;

		q7_t test_scratch_pad[TEST_OF*TEST_IN_DIM + 2*TEST_IF*TEST_K_DIM*TEST_OF];
		test_buffer = test_scratch_pad;
		test_col_buffer = test_buffer + TEST_OF*TEST_IN_DIM;
	#endif


	#ifdef TEST_BIT_DEPTH_16
		q15_t byte_array_large[TEST_IN_DIM*TEST_OF] = {-1};

		const q15_t test_wt_large[TEST_OF*TEST_IF*TEST_OF*TEST_K_DIM] = {-1};
		const q15_t test_bias_large[TEST_OF*TEST_IF*TEST_OF] = {-1};
		const q15_t test_final_fc_bias_large[TEST_DENSE] = {-1};

		q15_t* test_buffer_large;
		q15_t* test_col_buffer_large;

		q15_t test_scratch_pad_large[TEST_OF*TEST_IN_DIM + 2*TEST_IF*TEST_K_DIM*TEST_OF];
		test_buffer_large = test_scratch_pad_large;
		test_col_buffer_large = test_buffer_large + TEST_OF*TEST_IN_DIM;
	#endif

	for (;;)
	{
		evt = osMessageGet(sensorSetup[sensorId].proc2_queueId, osWaitForever);

		if (evt.status == osEventMessage)
		{
			rptr = (SensorsData *) evt.value.p;

			#ifdef TEST_BIT_DEPTH_8
				arm_convolve_HWC_q7_basic_nonsquare(byte_array, TEST_IN_DIM, 1, TEST_IF, test_wt, TEST_OF, TEST_K_DIM, 1, TEST_PADDING, 0, TEST_STRIDE, 1, test_bias, CONV1_BIAS_LSHIFT, CONV1_OUT_RSHIFT, test_buffer, TEST_IN_DIM, 1, (q15_t*)test_col_buffer, NULL);
				arm_relu_q7(test_buffer, TEST_IN_DIM*TEST_OF);
//				MaxPooling
				arm_convolve_HWC_q7_basic_nonsquare(byte_array, TEST_IN_DIM/2, 1, TEST_IF*10, test_wt, TEST_OF, TEST_K_DIM, 1, TEST_PADDING, 0, TEST_STRIDE, 1, test_bias, CONV1_BIAS_LSHIFT, CONV1_OUT_RSHIFT, test_buffer, TEST_IN_DIM/2, 1, (q15_t*)test_col_buffer, NULL);
				arm_relu_q7(test_buffer, TEST_IN_DIM*TEST_OF/2);
//				MaxPooling
				arm_fully_connected_q7(byte_array, test_final_fc_wt, (TEST_IN_DIM/4)*TEST_OF, TEST_DENSE, FINAL_FC_BIAS_LSHIFT, FINAL_FC_OUT_RSHIFT, test_final_fc_bias, test_buffer, (q15_t*)test_col_buffer);
				arm_relu_q7(test_buffer, TEST_IN_DIM*TEST_OF/4);
				arm_fully_connected_q7(byte_array, test_final_fc_wt, TEST_DENSE, 5, FINAL_FC_BIAS_LSHIFT, FINAL_FC_OUT_RSHIFT, test_final_fc_bias, test_buffer, (q15_t*)test_col_buffer);
				arm_softmax_q7(byte_array, 5, test_buffer);
			#endif

			#ifdef TEST_BIT_DEPTH_16
				arm_convolve_HWC_q15_basic_nonsquare(byte_array_large, TEST_IN_DIM, 1, TEST_IF, test_wt_large, TEST_OF, TEST_K_DIM, 1, TEST_PADDING, 0, TEST_STRIDE, 1, test_bias_large, CONV1_BIAS_LSHIFT, CONV1_OUT_RSHIFT, test_buffer_large, TEST_IN_DIM, 1, (q15_t*)test_col_buffer_large, NULL);
				arm_relu_q15(test_buffer_large, TEST_IN_DIM*TEST_OF);
//				MaxPooling
				arm_convolve_HWC_q15_basic_nonsquare(byte_array_large, TEST_IN_DIM/2, 1, TEST_IF*10, test_wt_large, TEST_OF, TEST_K_DIM, 1, TEST_PADDING, 0, TEST_STRIDE, 1, test_bias_large, CONV1_BIAS_LSHIFT, CONV1_OUT_RSHIFT, test_buffer_large, TEST_IN_DIM/2, 1, (q15_t*)test_col_buffer_large, NULL);
				arm_relu_q15(test_buffer_large, TEST_IN_DIM*TEST_OF/2);
//				MaxPooling
				arm_fully_connected_q15(byte_array_large, test_final_fc_wt_large, (TEST_IN_DIM/4)*TEST_OF, TEST_DENSE, FINAL_FC_BIAS_LSHIFT, FINAL_FC_OUT_RSHIFT, test_final_fc_bias_large, test_buffer_large, (q15_t*)test_col_buffer_large);
				arm_relu_q15(test_buffer_large, TEST_IN_DIM*TEST_OF/4);
				arm_fully_connected_q15(byte_array_large, test_final_fc_wt_large, TEST_DENSE, 5, FINAL_FC_BIAS_LSHIFT, FINAL_FC_OUT_RSHIFT, test_final_fc_bias_large, test_buffer_large, (q15_t*)test_col_buffer_large);
				arm_softmax_q15(byte_array_large, 5, test_buffer_large);
			#endif

//				if(rptr->gp_int_v[0] == 1) rptr->gp_int_v[0] = 0;
//				else rptr->gp_int_v[0] = 1;

			if (osMessagePut(sensorSetup[sensorId].out_proc2_queueId, (uint32_t) rptr, osWaitForever) != osOK) Error_Handler();
		}
	}
}

int ecg_data_threshold(SensorsData *data)
{
	return SEND;
}



void samp_timer_0_Callback(void const *arg) { osSemaphoreRelease(sensorSetup[SENSOR_0].get_semId); }

void send_timer_0_Callback(void const *arg) { if (osMessagePut(sensorSetup[SENSOR_0].thrsh_queueId, (uint32_t) sensorSetup[SENSOR_0].last_value, osWaitForever) != osOK) Error_Handler(); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_1: FUNCTIONS & TIMER

void temp_read_from_sensor(SensorsData *packet)
{
	packet->data_len = 	1;
	*((float*) packet->data) = 36.5;
}

int temp_data_threshold(SensorsData *data) { return SEND; }



void samp_timer_1_Callback(void const *arg) { osSemaphoreRelease(sensorSetup[SENSOR_1].get_semId); }

void send_timer_1_Callback(void const *arg) { if (osMessagePut(sensorSetup[SENSOR_1].thrsh_queueId, (uint32_t) sensorSetup[SENSOR_1].last_value, osWaitForever) != osOK) Error_Handler(); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SENSOR_2: FUNCTIONS

void pres_read_from_sensor(SensorsData *packet)
{
	packet->data_len = 	1;
	*((float*) packet->data) = 107.011;
}

int pres_data_threshold(SensorsData *data) { return SEND; }



void samp_timer_2_Callback(void const *arg) { osSemaphoreRelease(sensorSetup[SENSOR_2].get_semId); }

void send_timer_2_Callback(void const *arg) { if (osMessagePut(sensorSetup[SENSOR_2].thrsh_queueId, (uint32_t) sensorSetup[SENSOR_2].last_value, osWaitForever) != osOK) Error_Handler(); }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////









void init_all_task()
{

	int *message;

	message = (int *) osPoolAlloc(thread_poolId);
	*message = TASK_SETUP;

	if (osMessagePut(master_queueId, (uint32_t) message, osWaitForever) != osOK) Error_Handler();
}

void *dataMalloc(int type, int size)
{
	switch (type)
	{
	case INT16:
		return pvPortMalloc(sizeof(int16_t) * size);

	case INT32:
		return pvPortMalloc(sizeof(int32_t) * size);

	case FLOAT:
		return pvPortMalloc(sizeof(float) * size);
	}
}

void dataCopy(int type, int *cnt, SensorsData *src, SensorsData *dst)
{
	switch (type)
	{
	case INT16:
		*(((int16_t*) dst->data) + --(*cnt)) = *((int16_t*) src->data);
		break;

	case INT32:
		*(((int32_t*) dst->data) + --(*cnt)) = *((int32_t*) src->data);
		break;

	case FLOAT:
		*(((float*) dst->data) + --(*cnt)) = *((float*) src->data);
		break;
	}
}



void master_timer_Callback(void const *arg)
{
	int *message;

	message = (int *) osPoolAlloc(thread_poolId);
	*message = PERIOD_CHECK;
	osMessagePut(master_queueId, (uint32_t) message, osWaitForever);
}

void master_timer_Start(void)
{
	exec = 1;
	master_timerId = osTimerCreate(osTimer(master_timer), osTimerPeriodic, &exec);			// Create periodic timer
	if (master_timerId) osTimerStart(master_timerId, (float) timer_period_master);       	// start timer
}

void master_timer_Stop(void) { osTimerStop(master_timerId); }



void ble_timer_Callback(void const *arg) { osSemaphoreRelease(ble_semId); }

void ble_timer_Start(void)
{
	exec = 1;
	ble_timerId = 		osTimerCreate(osTimer(ble_timer), osTimerPeriodic, &exec);			// Create periodic timer
	if (ble_timerId) 	osTimerStart(ble_timerId, (float) timer_period_master);
}

void ble_timer_Stop(void) { osTimerStop(ble_timerId); }









static void GetBatteryInfoData(void)
{
//  uint32_t soc;
//  int32_t current= 0;
//	uint32_t voltage;

  uint8_t v_mode;

  BSP_GG_Task(STC3115_handle,&v_mode);		// Update Gas Gouge Status

  /* Read the Gas Gouge Status */
  BSP_GG_GetVoltage(STC3115_handle, &voltage);
  BSP_GG_GetCurrent(STC3115_handle, &current);
  BSP_GG_GetSOC(STC3115_handle, &soc);

  #ifdef DEBUG_USB_NOTIFY_TRAMISSION
  STLBLE_PRINTF("Charge= %ld%% Voltage=%ld mV Current= %ld mA\r\n", soc, voltage, current);
  #endif
}



static void InitBlueNRGStack(void)
{
	#ifdef DEBUG_USB_CONNECTION_INFO
	{
		STLBLE_PRINTF("STMicroelectronics %s:\r\n"
			"\tVersion %c.%c.%c\r\n"
			"\tSensorTile"
			"\r\n",
		STLBLE_PACKAGENAME, STLBLE_VERSION_MAJOR,STLBLE_VERSION_MINOR,STLBLE_VERSION_PATCH);

	STLBLE_PRINTF("\t(HAL %ld.%ld.%ld_%ld)\r\n" "\tCompiled %s %s"
	#if defined (__IAR_SYSTEMS_ICC__)
		" (IAR)\r\n"
	#elif defined (__CC_ARM)
		" (KEIL)\r\n"
	#elif defined (__GNUC__)
		" (openstm32)\r\n"
	#endif
			,HAL_GetHalVersion() >>24,
			(HAL_GetHalVersion() >>16)&0xFF,
			(HAL_GetHalVersion() >> 8)&0xFF,
			 HAL_GetHalVersion()      &0xFF,
			__DATE__,__TIME__);
	}
	#endif

	const char 		BoardName[8] = {NAME_STLBLE,0};
	uint16_t 		service_handle, dev_name_char_handle, appearance_char_handle;
	int 			ret;
	uint8_t  		hwVersion;
	uint16_t 		fwVersion;

	#ifdef STATIC_BLE_MAC
	uint8_t tmp_bdaddr[6] = {STATIC_BLE_MAC};
    for (uint8_t i = 0; i < 6; i++) bdaddr[i] = tmp_bdaddr[i];
	#endif


	BNRG_SPI_Init();  															// Initialize the BlueNRG SPI driver
	HCI_Init();																	// Initialize the BlueNRG HCI
	BlueNRG_RST();																// Reset BlueNRG hardware
	getBlueNRGVersion(&hwVersion, &fwVersion);									// Get the BlueNRG HW and FW versions

	if (hwVersion > 0x30) TargetBoardFeatures.bnrg_expansion_board = IDB05A1;	// X-NUCLEO-IDB05A1 expansion board is used
	else TargetBoardFeatures.bnrg_expansion_board = IDB04A1;					// X-NUCLEO-IDB0041 expansion board is used

	BlueNRG_RST();																// Reset BlueNRG again otherwise it will fail.

	#ifndef STATIC_BLE_MAC														// Create a Unique BLE MAC
    bdaddr[0] = (STM32_UUID[1]>>24)&0xFF;
    bdaddr[1] = (STM32_UUID[0]    )&0xFF;
    bdaddr[2] = (STM32_UUID[2] >>8)&0xFF;
    bdaddr[3] = (STM32_UUID[0]>>16)&0xFF;
    bdaddr[4] = (((STLBLE_VERSION_MAJOR-48)*10) + (STLBLE_VERSION_MINOR-48)+100)&0xFF;
    bdaddr[5] = 0xC0;															// For a Legal BLE Random MAC

	#else
    ret = aci_hal_write_config_data(CONFIG_DATA_PUBADDR_OFFSET, CONFIG_DATA_PUBADDR_LEN, bdaddr);
    if (ret)
    {
		#ifdef DEBUG_USB_CONNECTION_INFO
    	{
    		STLBLE_PRINTF("\r\nSetting Pubblic BD_ADDR failed\r\n");
    	}
		#endif
		goto fail;
	}
	#endif

	ret = aci_gatt_init();
	if (ret)
	{
		#ifdef DEBUG_USB_CONNECTION_INFO
    	{
    		STLBLE_PRINTF("\r\nGATT_Init failed\r\n");
        }
    	#endif
		goto fail;
	}

	if (TargetBoardFeatures.bnrg_expansion_board == IDB05A1)
	{
		ret = aci_gap_init_IDB05A1(GAP_PERIPHERAL_ROLE_IDB05A1, 0, 0x07, &service_handle, &dev_name_char_handle, &appearance_char_handle);
	}
	else
	{
		ret = aci_gap_init_IDB04A1(GAP_PERIPHERAL_ROLE_IDB04A1, &service_handle, &dev_name_char_handle, &appearance_char_handle);
	}

	if (ret != BLE_STATUS_SUCCESS)
	{
		#ifdef DEBUG_USB_CONNECTION_INFO
		{
			STLBLE_PRINTF("\r\nGAP_Init failed\r\n");
        }
    	#endif
		goto fail;
	}

	#ifndef  STATIC_BLE_MAC
	ret = hci_le_set_random_address(bdaddr);
	if (ret)
	{
		#ifdef DEBUG_USB_CONNECTION_INFO
		{
			STLBLE_PRINTF("\r\nSetting the Static Random BD_ADDR failed\r\n");
		}
		#endif
		goto fail;
	}
	#endif

	ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0, 7/*strlen(BoardName)*/, (uint8_t *) BoardName);
	if (ret)
	{
		#ifdef DEBUG_USB_CONNECTION_INFO
		{
			STLBLE_PRINTF("\r\naci_gatt_update_char_value failed\r\n");
		}
		#endif
		while (1);
	}

	ret = aci_gap_set_auth_requirement(MITM_PROTECTION_REQUIRED, OOB_AUTH_DATA_ABSENT, NULL, 7, 16, USE_FIXED_PIN_FOR_PAIRING, 123456, BONDING);
	if (ret != BLE_STATUS_SUCCESS)
	{
		#ifdef DEBUG_USB_CONNECTION_INFO
		{
			STLBLE_PRINTF("\r\nGAP setting Authentication failed\r\n");
		}
		#endif
		goto fail;
	}

	#ifdef DEBUG_USB_CONNECTION_INFO
	{
		STLBLE_PRINTF("SERVER: BLE Stack Initialized \r\n"
		"\t\tBoard type=%s HWver=%d, FWver=%d.%d.%c\r\n"
		"\t\tBoardName= %s\r\n"
		"\t\tBoardMAC = %x:%x:%x:%x:%x:%x\r\n\n",
		"SensorTile",
		hwVersion,
		fwVersion>>8,
		(fwVersion>>4)&0xF,
		(hwVersion > 0x30) ? ('a'+(fwVersion&0xF)-1) : 'a',
		BoardName,
		bdaddr[5],bdaddr[4],bdaddr[3],bdaddr[2],bdaddr[1],bdaddr[0]);
	}
	#endif

	aci_hal_set_tx_power_level(0,0);											// Set output power level

	return;

	fail:
		return;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
		case BNRG_SPI_EXTI_PIN:
			HCI_Isr();
			HCI_ProcessEvent=1;
			break;

		default:

			break;
	}
}



void powerControl(int mode, int frequency)
{

	switch (mode)
	{
		/**
		  * @brief Configure the main internal regulator output voltage.
		  * @param  VoltageScaling: specifies the regulator output voltage to achieve
		  *         a tradeoff between performance and power consumption.
		  *          This parameter can be one of the following values:
		  *            @arg @ref PWR_REGULATOR_VOLTAGE_SCALE1 Regulator voltage output range 1 mode,
		  *                                                typical output voltage at 1.2 V,
		  *                                                system frequency up to 80 MHz.
		  *            @arg @ref PWR_REGULATOR_VOLTAGE_SCALE2 Regulator voltage output range 2 mode,
		  *                                                typical output voltage at 1.0 V,
		  *                                                system frequency up to 26 MHz.
		  * @note  When moving from Range 1 to Range 2, the system frequency must be decreased to
		  *        a value below 26 MHz before calling HAL_PWREx_ControlVoltageScaling() API.
		  *        When moving from Range 2 to Range 1, the system frequency can be increased to
		  *        a value up to 80 MHz after calling HAL_PWREx_ControlVoltageScaling() API.
		  * @note  When moving from Range 2 to Range 1, the API waits for VOSF flag to be
		  *        cleared before returning the status. If the flag is not cleared within
		  *        50 microseconds, HAL_TIMEOUT status is reported.
		  * @retval HAL Status
		  */
		case RUN:

			HAL_PWREx_DisableLowPowerRunMode();
			if(frequency < 26)
			{
				HAL_RCC_DeInit();
				SystemClock_Config_adv(frequency);
//				ulTimerCountsForOneTick = ( ( SystemCoreClock ) / configTICK_RATE_HZ );

				HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE2);
			}
			else
			{
				HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

				HAL_RCC_DeInit();
				SystemClock_Config_adv_mod(frequency);
//				ulTimerCountsForOneTick = ( ( SystemCoreClock ) / configTICK_RATE_HZ );
			}

		break;



		/**
		  * @brief Enter Low-power Run mode
		  * @note  In Low-power Run mode, all I/O pins keep the same state as in Run mode.
		  * @note  When Regulator is set to PWR_LOWPOWERREGULATOR_ON, the user can optionally configure the
		  *        Flash in power-down monde in setting the RUN_PD bit in FLASH_ACR register.
		  *        Additionally, the clock frequency must be reduced below 2 MHz.
		  *        Setting RUN_PD in FLASH_ACR then appropriately reducing the clock frequency must
		  *        be done before calling HAL_PWREx_EnableLowPowerRunMode() API.
		  * @retval None
		  */
		case LPRUN:
			if(frequency == 1){
				HAL_RCC_DeInit();
				SystemClock_Config_lp();
				ulTimerCountsForOneTick = ( ( SystemCoreClock ) / configTICK_RATE_HZ );
				HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE2);
			}
			else{

				HAL_RCC_DeInit();
				SystemClock_Config_lp_2();
				ulTimerCountsForOneTick = ( ( SystemCoreClock ) / configTICK_RATE_HZ );
				HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE2);
			}
		break;



		/**
		  * @brief Enter Sleep or Low-power Sleep mode.
		  * @note  In Sleep/Low-power Sleep mode, all I/O pins keep the same state as in Run mode.
		  * @param Regulator: Specifies the regulator state in Sleep/Low-power Sleep mode.
		  *          This parameter can be one of the following values:
		  *            @arg @ref PWR_MAINREGULATOR_ON Sleep mode (regulator in main mode)
		  *            @arg @ref PWR_LOWPOWERREGULATOR_ON Low-power Sleep mode (regulator in low-power mode)
		  * @note  Low-power Sleep mode is entered from Low-power Run mode. Therefore, if not yet
		  *        in Low-power Run mode before calling HAL_PWR_EnterSLEEPMode() with Regulator set
		  *        to PWR_LOWPOWERREGULATOR_ON, the user can optionally configure the
		  *        Flash in power-down monde in setting the SLEEP_PD bit in FLASH_ACR register.
		  *        Additionally, the clock frequency must be reduced below 2 MHz.
		  *        Setting SLEEP_PD in FLASH_ACR then appropriately reducing the clock frequency must
		  *        be done before calling HAL_PWR_EnterSLEEPMode() API.
		  * @note  When exiting Low-power Sleep mode, the MCU is in Low-power Run mode. To move in
		  *        Run mode, the user must resort to HAL_PWREx_DisableLowPowerRunMode() API.
		  * @param SLEEPEntry: Specifies if Sleep mode is entered with WFI or WFE instruction.
		  *           This parameter can be one of the following values:
		  *            @arg @ref PWR_SLEEPENTRY_WFI enter Sleep or Low-power Sleep mode with WFI instruction
		  *            @arg @ref PWR_SLEEPENTRY_WFE enter Sleep or Low-power Sleep mode with WFE instruction
		  * @note  When WFI entry is used, tick interrupt have to be disabled if not desired as
		  *        the interrupt wake up source.
		  * @retval None
		  */
		case SLEEP_WFI:
			HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
		break;

		case SLEEP_WFE:
			HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFE);
		break;

		case LPSLEEP_WFI:
			HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
		break;

		case LPSLEEP_WFE:
			HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFE);
		break;



		/**
		  * @brief Enter Stop 0 mode.
		  * @note  In Stop 0 mode, main and low voltage regulators are ON.
		  * @note  In Stop 0 mode, all I/O pins keep the same state as in Run mode.
		  * @note  All clocks in the VCORE domain are stopped; the PLL, the MSI,
		  *        the HSI and the HSE oscillators are disabled. Some peripherals with the wakeup capability
		  *        (I2Cx, USARTx and LPUART) can switch on the HSI to receive a frame, and switch off the HSI
		  *        after receiving the frame if it is not a wakeup frame. In this case, the HSI clock is propagated
		  *        only to the peripheral requesting it.
		  *        SRAM1, SRAM2 and register contents are preserved.
		  *        The BOR is available.
		  * @note  When exiting Stop 0 mode by issuing an interrupt or a wakeup event,
		  *         the HSI RC oscillator is selected as system clock if STOPWUCK bit in RCC_CFGR register
		  *         is set; the MSI oscillator is selected if STOPWUCK is cleared.
		  * @note  By keeping the internal regulator ON during Stop 0 mode, the consumption
		  *         is higher although the startup time is reduced.
		  * @param STOPEntry  specifies if Stop mode in entered with WFI or WFE instruction.
		  *          This parameter can be one of the following values:
		  *            @arg @ref PWR_STOPENTRY_WFI  Enter Stop mode with WFI instruction
		  *            @arg @ref PWR_STOPENTRY_WFE  Enter Stop mode with WFE instruction
		  * @retval None
		  */
		case STOP0_WFI:
//			HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFI);
			HAL_PWREx_EnterSTOP0Mode(PWR_STOPENTRY_WFI);
		break;

		case STOP0_WFE:
//			HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFE);
			HAL_PWREx_EnterSTOP0Mode(PWR_STOPENTRY_WFE);
		break;



		/**
		  * @brief Enter Stop 1 mode.
		  * @note  In Stop 1 mode, only low power voltage regulator is ON.
		  * @note  In Stop 1 mode, all I/O pins keep the same state as in Run mode.
		  * @note  All clocks in the VCORE domain are stopped; the PLL, the MSI,
		  *        the HSI and the HSE oscillators are disabled. Some peripherals with the wakeup capability
		  *        (I2Cx, USARTx and LPUART) can switch on the HSI to receive a frame, and switch off the HSI
		  *        after receiving the frame if it is not a wakeup frame. In this case, the HSI clock is propagated
		  *        only to the peripheral requesting it.
		  *        SRAM1, SRAM2 and register contents are preserved.
		  *        The BOR is available.
		  * @note  When exiting Stop 1 mode by issuing an interrupt or a wakeup event,
		  *         the HSI RC oscillator is selected as system clock if STOPWUCK bit in RCC_CFGR register
		  *         is set; the MSI oscillator is selected if STOPWUCK is cleared.
		  * @note  Due to low power mode, an additional startup delay is incurred when waking up from Stop 1 mode.
		  * @param STOPEntry  specifies if Stop mode in entered with WFI or WFE instruction.
		  *          This parameter can be one of the following values:
		  *            @arg @ref PWR_STOPENTRY_WFI  Enter Stop mode with WFI instruction
		  *            @arg @ref PWR_STOPENTRY_WFE  Enter Stop mode with WFE instruction
		  * @retval None
		  */
		case STOP1_WFI:
//			HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
			HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFI);
		break;

		case STOP1_WFE:
//			HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFE);
			HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFE);
		break;



		/**
		  * @brief Enter Stop 2 mode.
		  * @note  In Stop 2 mode, only low power voltage regulator is ON.
		  * @note  In Stop 2 mode, all I/O pins keep the same state as in Run mode.
		  * @note  All clocks in the VCORE domain are stopped, the PLL, the MSI,
		  *        the HSI and the HSE oscillators are disabled. Some peripherals with wakeup capability
		  *        (LCD, LPTIM1, I2C3 and LPUART) can switch on the HSI to receive a frame, and switch off the HSI after
		  *        receiving the frame if it is not a wakeup frame. In this case the HSI clock is propagated only
		  *        to the peripheral requesting it.
		  *        SRAM1, SRAM2 and register contents are preserved.
		  *        The BOR is available.
		  *        The voltage regulator is set in low-power mode but LPR bit must be cleared to enter stop 2 mode.
		  *        Otherwise, Stop 1 mode is entered.
		  * @note  When exiting Stop 2 mode by issuing an interrupt or a wakeup event,
		  *         the HSI RC oscillator is selected as system clock if STOPWUCK bit in RCC_CFGR register
		  *         is set; the MSI oscillator is selected if STOPWUCK is cleared.
		  * @param STOPEntry  specifies if Stop mode in entered with WFI or WFE instruction.
		  *          This parameter can be one of the following values:
		  *            @arg @ref PWR_STOPENTRY_WFI  Enter Stop mode with WFI instruction
		  *            @arg @ref PWR_STOPENTRY_WFE  Enter Stop mode with WFE instruction
		  * @retval None
		  */

		case STOP2_WFI:
			HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
		break;

		case STOP2_WFE:
			HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFE);
		break;



		/**
		  * @brief Enter Standby mode.
		  * @note  In Standby mode, the PLL, the HSI, the MSI and the HSE oscillators are switched
		  *        off. The voltage regulator is disabled, except when SRAM2 content is preserved
		  *        in which case the regulator is in low-power mode.
		  *        SRAM1 and register contents are lost except for registers in the Backup domain and
		  *        Standby circuitry. SRAM2 content can be preserved if the bit RRS is set in PWR_CR3 register.
		  *        To enable this feature, the user can resort to HAL_PWREx_EnableSRAM2ContentRetention() API
		  *        to set RRS bit.
		  *        The BOR is available.
		  * @note  The I/Os can be configured either with a pull-up or pull-down or can be kept in analog state.
		  *        HAL_PWREx_EnableGPIOPullUp() and HAL_PWREx_EnableGPIOPullDown() respectively enable Pull Up and
		  *        Pull Down state, HAL_PWREx_DisableGPIOPullUp() and HAL_PWREx_DisableGPIOPullDown() disable the
		  *        same.
		  *        These states are effective in Standby mode only if APC bit is set through
		  *        HAL_PWREx_EnablePullUpPullDownConfig() API.
		  * @retval None
		  */
		case STANDBY:
			HAL_PWR_EnterSTANDBYMode();
		break;



		/**
		  * @brief Enter Shutdown mode.
		  * @note  In Shutdown mode, the PLL, the HSI, the MSI, the LSI and the HSE oscillators are switched
		  *        off. The voltage regulator is disabled and Vcore domain is powered off.
		  *        SRAM1, SRAM2 and registers contents are lost except for registers in the Backup domain.
		  *        The BOR is not available.
		  * @note  The I/Os can be configured either with a pull-up or pull-down or can be kept in analog state.
		  * @retval None
		  */
		case SHUTDOWN:
			HAL_PWREx_EnterSHUTDOWNMode();
		break;



		default:
			while(1);
		break;
	}
}

///**
//* @brief  Initialize all sensors
//* @param  None
//* @retval None
//*/
//static void initializeAcc( void )
//{
//	if (BSP_ACCELERO_Init( LSM303AGR_X_0, &LSM303AGR_X_0_handle ) != COMPONENT_OK)
//	{
//		  while(1);
//	}
//
//}
//
//
///**
// * @brief  Enable all sensors
// * @param  None
// * @retval None
// */
// void enableAcc( void )
// {
//   BSP_ACCELERO_Sensor_Enable( LSM303AGR_X_0_handle );
// }
//
//
// /**
// * @brief  Set ODR all sensors
// * @param  None
// * @retval None
// */
// void setOdrAcc( void )
// {
//   BSP_ACCELERO_Set_ODR_Value( LSM303AGR_X_0_handle, ACCELERO_ODR);
// }
//
//
// /**
// * @brief  Disable all sensors
// * @param  None
// * @retval None
// */
// void disableAcc( void )
// {
//   BSP_ACCELERO_Sensor_Disable( LSM303AGR_X_0_handle );
// }



void Error_Handler(void) { while (1) {} }



#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */



void assert_failed(uint8_t *file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{}
}
#endif